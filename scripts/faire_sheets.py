import fnmatch
import csv
import os

bam_path = '/proj/mckaylab/users/mniederhuber/FAIRE_timecourse/Bam/'
peak_path = '/proj/mckaylab/users/mniederhuber/FAIRE_timecourse/Peaks/'

with open('sample_sheet.tsv', 'w') as sht:
	samp = csv.writer(sht, delimiter = '\t')
	samp.writerow(['genotype', 'tissue', 'time','rep', 'bam_file'])

	for root, dirnames, filenames in os.walk(bam_path):
		for filename in fnmatch.filter(filenames, '*dupsRemoved_sorted.bam'):
			match = os.path.abspath(os.path.join(root, filename))
			fn = filename.split(sep = '_')
			samp.writerow([fn[0],fn[2],fn[1],fn[4],match])

with open('peak_sheet.tsv', 'w') as psht:
	peak = csv.writer(psht, delimiter = '\t')
	peak.writerow(['genotype','tissue', 'time', 'rep', 'peak_file'])
	for root, dirnames, filenames in os.walk(peak_path):
		for filename in fnmatch.filter(filenames, '*.narrowPeak'):
			np = os.path.abspath(os.path.join(root, filename))
			fn = filename.split(sep = '_')
			peak.writerow([fn[0], fn[2], fn[1], fn[4], np])

				
